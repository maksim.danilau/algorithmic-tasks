package by.danilau.task01;

class RangeMergerV1 {

    static int[][] merge(int[][] input) {
        for (int rangeIndex = 0; rangeIndex < input.length; rangeIndex++) {
            int[] range = input[rangeIndex];
            int rangeStart = range[0];
            int rangeEnd = range[1];

            for (int mergeCandidateIndex = rangeIndex + 1; mergeCandidateIndex < input.length; mergeCandidateIndex++) {
                int[] mergeCandidate = input[mergeCandidateIndex];
                int mergeCandidateStart = mergeCandidate[0];
                int mergeCandidateEnd = mergeCandidate[1];
                if ((mergeCandidateStart > rangeStart && mergeCandidateStart <= rangeEnd)
                        || (mergeCandidateStart < rangeStart && mergeCandidateEnd >= rangeStart)) {
                    int[] mergedRange = {Math.min(rangeStart, mergeCandidateStart), Math.max(rangeEnd, mergeCandidateEnd)};

                    int[][] result = new int[input.length - 1][];
                    result[0] = mergedRange;
                    int newPositionIndex = 1;
                    for (int oldPositionIndex = 0; oldPositionIndex < input.length; oldPositionIndex++) {
                        if (oldPositionIndex == rangeIndex || oldPositionIndex == mergeCandidateIndex) continue;
                        result[newPositionIndex] = input[oldPositionIndex];
                        newPositionIndex++;
                    }
                    return merge(result);
                }
            }
        }
        return input;
    }
}
