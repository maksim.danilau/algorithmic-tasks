package by.danilau.task01;

import by.danilau.util.Range;

public class RangeMergerV3 {

    static int[][] merge(int[][] input) {
        int rangesNumber = input.length;
        Range[] ranges = new Range[rangesNumber];
        for (int index = 0; index < rangesNumber; index++) {
            ranges[index] = new Range(input[index][0], input[index][1]);
        }

        for (int rangeIndex = 0; rangeIndex < rangesNumber; rangeIndex++) {
            Range range = ranges[rangeIndex];

            for (int mergeCandidateIndex = rangeIndex + 1; mergeCandidateIndex < rangesNumber; mergeCandidateIndex++) {
                Range mergeCandidate = ranges[mergeCandidateIndex];
                if (range.intersects(mergeCandidate)) {
                    ranges[0] = range.merge(mergeCandidate);
                    int newPositionIndex = 1;
                    for (int oldPositionIndex = 0; oldPositionIndex < rangesNumber; oldPositionIndex++) {
                        if (oldPositionIndex == rangeIndex || oldPositionIndex == mergeCandidateIndex) continue;
                        ranges[newPositionIndex] = ranges[oldPositionIndex];
                        newPositionIndex++;
                    }
                    rangesNumber--;
                    rangeIndex--;
                    break;
                }
            }
        }

        int[][] res = new int[rangesNumber][];
        for (int i = 0; i < rangesNumber; i++) {
            res[i] = new int[]{ranges[i].getStart(), ranges[i].getEnd()};
        }
        return res;
    }
}
