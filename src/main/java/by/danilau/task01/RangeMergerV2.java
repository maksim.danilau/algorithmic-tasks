package by.danilau.task01;

class RangeMergerV2 {

    static int[][] merge(int[][] input) {
        int rangesNumber = input.length;

        for (int rangeIndex = 0; rangeIndex < rangesNumber; rangeIndex++) {
            int[] range = input[rangeIndex];
            int rangeStart = range[0];
            int rangeEnd = range[1];

            for (int mergeCandidateIndex = rangeIndex + 1; mergeCandidateIndex < rangesNumber; mergeCandidateIndex++) {
                int[] mergeCandidate = input[mergeCandidateIndex];
                int mergeCandidateStart = mergeCandidate[0];
                int mergeCandidateEnd = mergeCandidate[1];
                if ((mergeCandidateStart > rangeStart && mergeCandidateStart <= rangeEnd)
                        || (mergeCandidateStart < rangeStart && mergeCandidateEnd >= rangeStart)) {
                    int[] mergedRange = {Math.min(rangeStart, mergeCandidateStart), Math.max(rangeEnd, mergeCandidateEnd)};

                    input[0] = mergedRange;
                    int newPositionIndex = 1;
                    for (int oldPositionIndex = 0; oldPositionIndex < rangesNumber; oldPositionIndex++) {
                        if (oldPositionIndex == rangeIndex || oldPositionIndex == mergeCandidateIndex) continue;
                        input[newPositionIndex] = input[oldPositionIndex];
                        newPositionIndex++;
                    }
                    rangeIndex--;
                    rangesNumber--;
                }
            }
        }

        int[][] res = new int[rangesNumber][];
        System.arraycopy(input, 0, res, 0, res.length);
        return res;
    }
}
