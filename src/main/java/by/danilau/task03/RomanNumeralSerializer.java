package by.danilau.task03;

import static by.danilau.task03.RomanNumeral.*;

public class RomanNumeralSerializer {

    private static final RomanNumeral[] anchors = new RomanNumeral[]{M, D, C, L, X, V, I};

    public static Integer deserialize(String romanNumber) {
        int result = 0;
        RomanNumeral prevMax = null;
        for (int charIndex = romanNumber.length() - 1; charIndex >= 0; charIndex--) {
            RomanNumeral curr = RomanNumeral.valueOf(romanNumber.substring(charIndex, charIndex + 1));
            int value = curr.toInt();

            if (prevMax != null && curr.compareTo(prevMax) < 0) {
                result -= value;
            } else {
                result += value;
            }

            if (prevMax == null || curr.compareTo(prevMax) > 0) {
                prevMax = curr;
            }
        }
        return result;
    }

    public static String serialize(Integer integer) {
        StringBuilder builder = new StringBuilder();
        for (int index = 0; index < anchors.length; index++) {
            RomanNumeral currAnchor = anchors[index];
            if (currAnchor == D && integer >= 900) {
                currAnchor = M;
            } else if (currAnchor == L && integer >= 90) {
                currAnchor = C;
            } else if (currAnchor == V && integer == 9) {
                currAnchor = X;
            }
            int res = integer / currAnchor.toInt();
            if (res > 0) {
                int i = 0;
                while (i < res) {
                    builder.append(currAnchor);
                    i++;
                }
                integer -= res * currAnchor.toInt();
            } else {
                RomanNumeral nextAnchor;
                if (currAnchor == X) {
                    nextAnchor = I;
                } else if (currAnchor == M) {
                    nextAnchor = C;
                } else if (currAnchor == C) {
                    nextAnchor = X;
                } else {
                    nextAnchor = anchors[index + 1];
                }
                int res2 = integer / (currAnchor.toInt() - nextAnchor.toInt());
                if (res2 == 1) {
                    builder.append(nextAnchor);
                    builder.append(currAnchor);
                    integer -= currAnchor.toInt() - nextAnchor.toInt();
                }
            }
            if (integer == 0) break;
        }
        return builder.toString();
    }
}
