package by.danilau.task03;

import java.util.stream.Stream;

public class RomanNumeralAddition {

    public static String add(String... numbers) {
        Integer result = Stream.of(numbers).mapToInt(RomanNumeralSerializer::deserialize).sum();
        return RomanNumeralSerializer.serialize(result);
    }
}
