package by.danilau.task02;

public class RangesIntersectionV1 {

    static boolean isLeftIntersectsRight(int[][] lefts, int[][] rights) {
        for (int[] leftRange : lefts) {
            int leftStart = leftRange[0];
            int leftEnd = leftRange[1];

            for (int[] rightRange : rights) {
                int rightStart = rightRange[0];
                int rightEnd = rightRange[1];

                boolean equal = (leftStart == rightStart) && (leftEnd == rightEnd);
                if (equal) return true;
            }
        }
        return false;
    }

}
