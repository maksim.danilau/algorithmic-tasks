package by.danilau.task02;

import by.danilau.util.Range;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class RangesIntersectionV4 {

    static boolean isLeftIntersectsRight(List<Range> lefts, List<Range> rights) {
        Map<Range, AtomicInteger> counter = new HashMap<>();

        lefts.forEach(range -> {
            counter.putIfAbsent(range, new AtomicInteger(0));
            counter.get(range).addAndGet(1);
        });

        rights.forEach(range -> {
            counter.putIfAbsent(range, new AtomicInteger(0));
            counter.get(range).addAndGet(1);
        });

        return counter.values().stream().anyMatch(count -> count.get() > 1);
    }
}
