package by.danilau.task02;

import by.danilau.util.Range;

import java.util.List;

public class RangesIntersectionV2 {

    static boolean isLeftIntersectsRight(List<Range> lefts, List<Range> rights) {
        for (Range leftRange : lefts) {
            for (Range rightRange : rights) {
                if (leftRange.equals(rightRange)) {
                    return true;
                }
            }
        }
        return false;
    }
}
