package by.danilau.task02;

import by.danilau.util.Range;

import java.util.List;

public class RangesIntersectionV3 {

    static boolean isLeftIntersectsRight(List<Range> lefts, List<Range> rights) {
        return lefts.stream().anyMatch(left -> rights.stream().anyMatch(right -> right.equals(left)));
    }
}
