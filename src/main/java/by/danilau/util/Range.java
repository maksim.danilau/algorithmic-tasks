package by.danilau.util;

import java.util.Objects;

public class Range {
    private int start;
    private int end;

    public Range(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public boolean intersects(Range range) {
        return (range.start > this.start && range.start <= this.end) || (range.start < this.start && range.end >= this.start);
    }

    public Range merge(Range range) {
        return new Range(Math.min(this.start, range.start), Math.max(this.end, range.end));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != Range.class) {
            return false;
        }
        Range range = (Range) obj;
        return (range.start == this.start) && (range.end == this.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, end);
    }
}
