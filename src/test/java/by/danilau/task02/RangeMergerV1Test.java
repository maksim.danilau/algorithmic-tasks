package by.danilau.task02;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RangeMergerV1Test {


    @Test
    void test_merge_case1() {
        int[][] inputLefts = {{0, 2}, {8, 10}, {1, 4}, {4, 6}};
        int[][] inputRights = {{0, 6}, {8, 10}};
        boolean actual = RangesIntersectionV1.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertTrue(actual);
    }

    @Test
    void test_merge_case2() {
        int[][] inputLefts = {{0, 6}, {8, 10}};
        int[][] inputRights = {{0, 6}, {8, 10}};
        boolean actual = RangesIntersectionV1.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertTrue(actual);
    }

    @Test
    void test_merge_case3() {
        int[][] inputLefts = {{1, 3}, {8, 10}, {0, 5}};
        int[][] inputRights = {{0, 6}};
        boolean actual = RangesIntersectionV1.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertFalse(actual);
    }
}
