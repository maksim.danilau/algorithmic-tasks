package by.danilau.task02;


import by.danilau.util.Range;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class RangeMergerV2Test {


    @Test
    void test_merge_case1() {
        List<Range> inputLefts = Arrays.asList(new Range(0, 2), new Range(8, 10), new Range(1, 4), new Range(4, 6));
        List<Range> inputRights = Arrays.asList(new Range(0, 6), new Range(8, 10));
        boolean actual = RangesIntersectionV2.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertTrue(actual);
    }

    @Test
    void test_merge_case2() {
        List<Range> inputLefts = Arrays.asList(new Range(0, 6), new Range(8, 10));
        List<Range> inputRights = Arrays.asList(new Range(0, 6), new Range(8, 10));
        boolean actual = RangesIntersectionV2.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertTrue(actual);
    }

    @Test
    void test_merge_case3() {
        List<Range> inputLefts = Arrays.asList(new Range(1, 3), new Range(8, 10), new Range(0, 5));
        List<Range> inputRights = Arrays.asList(new Range(0, 6));
        boolean actual = RangesIntersectionV2.isLeftIntersectsRight(inputLefts, inputRights);

        Assertions.assertFalse(actual);
    }
}
