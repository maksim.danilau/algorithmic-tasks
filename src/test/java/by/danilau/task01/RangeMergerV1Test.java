package by.danilau.task01;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RangeMergerV1Test {


    @Test
    void test_merge_case1() {
        int[][] input = {{0, 2}, {8, 10}, {1, 4}, {4, 6}};
        int[][] expected = {{0, 6}, {8, 10}};
        int[][] actual = RangeMergerV1.merge(input);

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void test_merge_case2() {
        int[][] input = {{0, 6}, {8, 10}};
        int[][] expected = {{0, 6}, {8, 10}};
        int[][] actual = RangeMergerV1.merge(input);

        Assertions.assertArrayEquals(expected, actual);
    }

    @Test
    void test_merge_case3() {
        int[][] input = {{1, 3}, {8, 10}, {0, 5}};
        int[][] expected = {{0, 5}, {8, 10}};
        int[][] actual = RangeMergerV1.merge(input);

        Assertions.assertArrayEquals(expected, actual);
    }
}
