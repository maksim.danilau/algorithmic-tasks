package by.danilau.task03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RomanNumeralAdditionTest {

    @Test
    void tests() {
        testAddition("II", "I", "I");
        testAddition("III", "II", "I");
        testAddition("MMMCMXLV", "CCCXXXIII", "MMMDCXII");
    }

    static void testAddition(String expected, String... numbers) {
        String actual = RomanNumeralAddition.add(numbers);

        Assertions.assertEquals(expected, actual);
    }
}
