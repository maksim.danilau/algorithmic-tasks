package by.danilau.task03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RomanNumeralParserTest {

    @Test
    public void test_deserilization() {
        test_deserilization("I", 1);
        test_deserilization("II", 2);
        test_deserilization("III", 3);
        test_deserilization("IV", 4);
        test_deserilization("V", 5);
        test_deserilization("VI", 6);
        test_deserilization("VII", 7);
        test_deserilization("VIII", 8);
        test_deserilization("IX", 9);
        test_deserilization("X", 10);
        test_deserilization("XI", 11);
        test_deserilization("XII", 12);
        test_deserilization("MCMXII", 1912);
        test_deserilization("MMXIX", 2019);
        test_deserilization("CCVII", 207);
    }

    @Test
    public void test_serilization() {
        test_serialization("I", 1);
        test_serialization("II", 2);
        test_serialization("III", 3);
        test_serialization("IV", 4);
        test_serialization("V", 5);
        test_serialization("VI", 6);
        test_serialization("VII", 7);
        test_serialization("VIII", 8);
        test_serialization("IX", 9);
        test_serialization("X", 10);
        test_serialization("XI", 11);
        test_serialization("XII", 12);
        test_serialization("MCMXII", 1912);
        test_serialization("MMXIX", 2019);
        test_serialization("CCVII", 207);
    }

    void test_deserilization(String input, Integer expected) {
        Integer actual = RomanNumeralSerializer.deserialize(input);

        Assertions.assertEquals(expected, actual);
    }

    void test_serialization(String expected, Integer input) {
        String actual = RomanNumeralSerializer.serialize(input);

        Assertions.assertEquals(expected, actual);
    }
}
